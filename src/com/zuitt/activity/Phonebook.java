package com.zuitt.activity;
import java.util.ArrayList;

public class Phonebook {
    private ArrayList<Contact> contacts;

    public Phonebook(){

    };

    public Phonebook(ArrayList<Contact> contacts){
        this.contacts= contacts;
    }

    public ArrayList<Contact> getPhoneContacts(){
        return this.contacts;
    }

    public void setPhoneContacts(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }


}
