import java.util.ArrayList;
import com.zuitt.activity.*;
public class Main {
    public static void main(String[] args) {
        ArrayList<Contact> contacts = new ArrayList<Contact>();

        Phonebook phonebook = new Phonebook(contacts);
        //1 Instantiate Contacts & add Contacts
        Contact contact1 = new Contact("John Doe", "+639152468596", "Quezon City");
        contacts.add(contact1);
        //2 Instantiate Contacts & add Contacts
        Contact contact2 = new Contact("Jane Doe", "+639162148573", "Caloocan City");
        contacts.add(contact2);


        if (phonebook.getPhoneContacts().isEmpty()) {

            System.out.println("Phonebook is empty Pls Add More Contacts");

        } else {

            for (Contact contact : phonebook.getPhoneContacts()) {
                System.out.println("----------------------------------------------");
                System.out.println(contact.getName());
                System.out.println("----------------------------------------------");
                System.out.println(contact.getName() + " has the following registered number:");
                System.out.println(contact.getContactNumber());
                System.out.println(contact.getName() + " has the following registered address:");
                System.out.println("I live in " + contact.getAddress());
            }

        }
    }
}